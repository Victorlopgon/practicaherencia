#pragma once
#include "LanchaPadre.h"
#include <iostream>


using namespace std;

class LanchaHija2 : public LanchaPadre
{
private:

	int velocidadExtra;

public:

	LanchaHija2(std::string pNombre, int pDistancia, int pVelocidad, int pvelocidadExtra);

	//Getters y Setters

	int getvelocidadExtra();
	void setvelocidadExtra(int pvelocidadExtra);

	void printarlanchaHija2();

};
