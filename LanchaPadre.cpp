#include <iostream>
#include "LanchaPadre.h"


//Constructor
LanchaPadre::LanchaPadre(int pVelocidad, int pDistancia, std::string pNombre) {

	nombre = pNombre;
	distancia = pDistancia;
	velocidad = pVelocidad;
}

//Getters 
std::string LanchaPadre::getNombre() {
	return nombre;
}

int LanchaPadre::getDistancia()
{
	return distancia;
}


int LanchaPadre::getVelocidad()
{
	return velocidad;
}


//Setters

void LanchaPadre::setNombre(std::string pNombre)
{
	nombre = pNombre;
}

void LanchaPadre::setDistancia(int pDistancia)
{
	distancia = pDistancia;
}



void LanchaPadre::setVelocidad(int pVelocidad)
{
	velocidad = pVelocidad;
}

//Metodo propio 

void LanchaPadre::printarlanchas() {

	std::cout << " La lancha que la controla " << nombre << " tiene una distancia recorrida de " << distancia << " actualmente una velocidad " << velocidad << "\n";

}
