#include "LanchaHija2.h"





LanchaHija2::LanchaHija2(std::string pNombre, int pDistancia, int pVelocidad, int pvelocidadExtra) : LanchaPadre(pVelocidad, pDistancia, pNombre)
{
	velocidadExtra = pvelocidadExtra;
}


int LanchaHija2::getvelocidadExtra()
{
	return velocidadExtra;
}

void LanchaHija2::setvelocidadExtra(int pvelocidadExtra)
{
	velocidadExtra = pvelocidadExtra;
}


void LanchaHija2::printarlanchaHija2() {

	std::cout << " Tiene una velocidad extra de " << getvelocidadExtra() << "\n";

}
