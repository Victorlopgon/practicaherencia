#pragma once
#include <string>

class LanchaPadre
{
private:
	int velocidad;
	int distancia;
	std::string nombre;

public:

	//constructor
	LanchaPadre(int pVelocidad, int pDistancia, std::string pNombre);


	//getters
	std::string getNombre();
	int getDistancia();
	int getVelocidad();

	//setters
	void setNombre(std::string pNombre);
	void setDistancia(int pDistancia);
	void setVelocidad(int pVelocidad);

	//metodo
	void printarlanchas();




};