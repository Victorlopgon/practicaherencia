// superMario.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include <windows.h>
#include "LanchaPadre.h"
#include "LanchaHija.h"
#include "LanchaHija2.h"

using namespace std;


//Stats NOMBRES
string nombreJugador = "Mario";
string nombreEnemigo1 = "Hermano Martillo";
string nombreEnemigo2 = "Bowser";



//TURNOS LEVEL1 
int minturnosinlevel1 = 1;
int maxturnosinlevel1 = 5;

//TURNOS LEVEL2
int minturnosinlevel2 = 1;
int maxturnosinlevel2 = 3;


//DADOS 
int dadoJugador;
int dadoCpu;
//VELOCIDADES
int velocidadJugador;
int velocidadCpu;
int velocidadExtra;
//DISTANCIAS 
int DistanciaJugador;
int DistanciaCpu;




void gameStart() {
    cout << "|     SUPERMARIOBOATS    |\n";
    cout << "|________________________|\n";
    cout << "|                        |\n";
    cout << "|    Sera capaz Mario    |\n";
    cout << "|     de GANAR a los     |\n";
    cout << "|     enemigos en las    |\n";
    cout << "|     lanchas?           |\n";
    cout << "|                        |\n";
    cout << "|       --Enemigos--     |\n";
    cout << "|     Hermano martillo W  Para ganar tiene que hacer 25 puntos Mario sino perdera            |\n";
    cout << "|          bowser      WW Para ganar la batalla final debera de hacer 40 puntos sino perdera |\n";
    cout << "|                                                                                            |\n";
    cout << "|                                                                                            |\n";
    cout << "|____________________________________________________________________________________________|\n";

}

void lanzamientodados() {
    cout << "se van a lanzar los dados \n";
    cout << "El jugador " << nombreJugador << " ha obtenido una puntuacion de " << dadoJugador << " y " << "el enemigo" << " ha obtenido una puntuacion de " << dadoCpu << "\n";

}

void MensajeBatallaFinal() {
    cout << "Mario ira a la batalla final donde se enfrentara a Bowser      \n";
    cout << "                                                               \n";
    cout << "     Acuerdate de que Mario debera de ganar con 40 puntos      \n";
    cout << "       Aunque tenga 40 puntos pero Bowser los consigue         \n";
    cout << "                       Mario perdera                           \n";

}


int main() {
    srand(time(NULL));
    system(" Color 5A");
    gameStart();
    Sleep(7000);
    system("cls");
   
    LanchaPadre lanchasJugador(15, 10, nombreJugador);
    LanchaHija lanchascpu(nombreEnemigo1, 10,5,5);
    LanchaHija2 lanchascpu2(nombreEnemigo2, 10,10,10);
   

    while (minturnosinlevel1 < maxturnosinlevel1) {


        lanzamientodados();
        dadoJugador = 1 + rand() % 11;
        dadoCpu = 1 + rand() % 11;



        lanchasJugador.printarlanchas();
        Sleep(2000);
        lanchascpu.printarlanchas();
        lanchascpu.printarlanchaHija();

        velocidadJugador = lanchasJugador.getVelocidad();
        lanchasJugador.setVelocidad(velocidadJugador + dadoJugador);

        velocidadCpu = lanchascpu.getVelocidad();
        lanchascpu.setVelocidad(velocidadCpu + dadoCpu + velocidadExtra);

        DistanciaJugador = lanchasJugador.getDistancia();
        lanchasJugador.setDistancia(DistanciaJugador + velocidadJugador * 100);

        DistanciaCpu = lanchasJugador.getDistancia();
        lanchascpu.setDistancia(DistanciaCpu + velocidadCpu * 100);

        velocidadExtra = lanchascpu.getvelocidadExtra();
        lanchascpu.setvelocidadExtra(velocidadCpu + 5);


        minturnosinlevel1++;

       

    }
    if (velocidadJugador > 25) {
        cout << "Ha ganado Mario y pasa al siguiente nivel " << "\n";
    }
    else {
        cout << "Ha ganado " << nombreEnemigo1 << "\n";
        exit(2);
    }

    Sleep(5000);
    system("cls");
    Sleep(1000);
    MensajeBatallaFinal();
    Sleep(5000);
    system("cls");
    Sleep(10000);
   
    while (minturnosinlevel2 < maxturnosinlevel2) {


       lanzamientodados();
       dadoJugador = 1 + rand() % 11;
       dadoCpu = 1 + rand() % 11;


       lanchasJugador.printarlanchas();
       Sleep(5000);
       lanchascpu2.printarlanchas();
       lanchascpu2.printarlanchaHija2();

       velocidadJugador = lanchasJugador.getVelocidad();
       lanchasJugador.setVelocidad(velocidadJugador + dadoJugador);

       velocidadCpu = lanchascpu.getVelocidad();
       lanchascpu2.setVelocidad(velocidadCpu + dadoCpu + velocidadExtra);

       DistanciaJugador = lanchasJugador.getDistancia();
       lanchasJugador.setDistancia(DistanciaJugador + velocidadJugador * 100);

       DistanciaCpu = lanchasJugador.getDistancia();
       lanchascpu2.setDistancia(DistanciaCpu + velocidadCpu * 100);

       velocidadExtra = lanchascpu.getvelocidadExtra();
       lanchascpu2.setvelocidadExtra(velocidadCpu + 13);


       minturnosinlevel2++;

    }
    if (velocidadJugador > 40) {
        cout << "Ha ganado Mario y ha conseguido vencer a los enemigos" << "\n";
    }
    else {
        cout << "Ha ganado Bowser y as perdido " <<"\n";
        exit(2);
    }

    return 0;

}


// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
