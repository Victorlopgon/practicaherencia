#include "LanchaPadre.h"
#include <iostream>


using namespace std;

class LanchaHija : public LanchaPadre
{
private:

	int velocidadExtra;

public:

	LanchaHija(std::string pNombre, int pDistancia, int pVelocidad, int pvelocidadExtra);

	//Getters y Setters

	int getvelocidadExtra();
	void setvelocidadExtra(int pvelocidadExtra);

	void printarlanchaHija();

};
